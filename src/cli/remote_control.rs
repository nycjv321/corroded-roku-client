extern crate ncurses;

use ncurses::*;

use roku::remote::RokuRemote;
use std::process::exit;

static ESCAPE: i32 = 0x1B;
static CTRL_C: i32 = 0x3;
static SPACE: i32 = 0x20;
static QUIT: i32 = 0x71;


pub struct RemoteControlWindowBuilder {
  max_y: i32,
  max_x: i32,

  start_y: i32,
  start_x: i32,
  directions: Directions,
}

pub struct RemoteControlWindow<'a> {
  window: WINDOW,
  remote: &'a RokuRemote<'a>,
}

pub struct Directions {
  start_y: i32,
  start_x: i32,
}

fn button(y: i32, x: i32, text: &str) -> WINDOW {
  let button = newwin(3, 7, y, x);
  box_(button, 0, 0);
  mvwprintw(button, 1, 3, text);
  wrefresh(button);
  return button;
}

fn global_message(message: &str) {
  mvprintw(0, 0, message);
}

impl Directions {
  pub fn left(&self) -> WINDOW {
    return button(self.start_y + 8, self.start_x + 2, "<");
  }
  pub fn right(&self) -> WINDOW {
    return button(self.start_y + 8, self.start_x + 18, ">");
  }
  pub fn down(&self) -> WINDOW {
    return button(self.start_y + 11, self.start_x + 10, "V");
  }
  pub fn up(&self) -> WINDOW {
    return button(self.start_y + 5, self.start_x + 10, "^");
  }
}

impl<'a> RemoteControlWindow<'a> {
  pub fn exit(&self) {
    endwin();
  }

  pub fn read_user_input(&self) {
    let mut ch = getch();
    let remote = &self.remote;
    while ch != ESCAPE && ch != CTRL_C && ch != QUIT {
      match ch
      {
        0x68 | 0x48 => {
          global_message("home pressed\t\t\t\t");
          remote.home()
        }
        0x7F | 0x42 | 0x62 => {
          global_message("back pressed\t\t\t\t");
          remote.back()
        }
        0xA | 0x20 => {
          global_message("enter pressed\t\t\t\t");
          remote.enter()
        }
        0x77 | KEY_UP => {
          global_message("up arrow key pressed\t\t\t\t");
          remote.up();
        }
        0x73 | KEY_DOWN => {
          global_message("down arrow key pressed\t\t\t\t");
          remote.down();
        }
        0x61 | KEY_LEFT => {
          global_message("left arrow key pressed\t\t\t\t");
          remote.left();
        }
        0x64 | KEY_RIGHT => {
          global_message("right arrow key pressed\t\t\t\t");
          remote.right();
        }
        0x79 => {
          global_message("pressed on youtube\t\t\t\t");
          remote.youtube();
        }
        0x6E => {
          global_message("pressed on netflix\t\t\t\t");
          remote.netflix();
        }
        0x69 => {
          global_message("pressed info\t\t\t\t");
          remote.info();
        }
        0x72 | 0x19a => {
          self.destroy_win(self.window);
          self.exit();
          let ui = RemoteControlWindowBuilder::initialize().build(self.remote);
          ui.read_user_input();
          ui.exit();
          exit(0);
        }
        _ => {
          // println!("{:#x?}", ch)
        }
      }
      ch = getch();
    }
  }

  fn destroy_win(&self, win: WINDOW) {
    let ch = ' ' as chtype;
    mvwprintw(win, 22, 2, "           ");
    mvwprintw(win, 21, 1, "                        ");

    wborder(win, ch, ch, ch, ch, ch, ch, ch, ch);
    wrefresh(win);
    delwin(win);
  }
}

impl RemoteControlWindowBuilder {
  pub fn initialize() -> RemoteControlWindowBuilder {
    initscr();
    raw();

    /* Allow for extended keyboard (like F1). */
    keypad(stdscr(), true);
    noecho();

    /* Invisible cursor. */
    curs_set(CURSOR_VISIBILITY::CURSOR_INVISIBLE);

    /* Status/help info. */
    refresh();

    /* Get the screen bounds. */
    let mut max_x = 0;
    let mut max_y = 0;
    getmaxyx(stdscr(), &mut max_y, &mut max_x);

    /* Start in the center. */
    let mut start_y = (max_y - super::WINDOW_HEIGHT) / 2;
    let mut start_x = (max_x - super::WINDOW_WIDTH) / 2;

    let directions = Directions { start_y, start_x };
    let remote_control_windows = RemoteControlWindowBuilder { max_x, max_y, start_y, start_x, directions };
    return remote_control_windows;
  }

  pub fn build<'a>(&self, remote: &'a RokuRemote) -> RemoteControlWindow<'a> {
    let window = self.create_win(remote);
    RemoteControlWindow { window, remote }
  }

  fn create_win(&self, remote: &RokuRemote) -> WINDOW {
    let win = newwin(super::WINDOW_HEIGHT, super::WINDOW_WIDTH, self.start_y, self.start_x);
    box_(win, 0, 0);
    global_message(format!("connected to \"{}\"", String::from(remote.client.url.clone())).as_str());


    mvwprintw(win, 22, 2, "crc v0.0.11");
    mvwprintw(win, 21, 1, "________________________");

    wrefresh(win);
    self.back_button();
    self.home();
    self.ok();

    let directions = &self.directions;
    directions.up();
    directions.left();
    directions.right();
    directions.down();

    self.info();
    self.netflix();
    self.youtube();
    win
  }

  fn back_button(&self) -> WINDOW {
    return button(self.start_y + 2, self.start_x + 2, "B");
  }

  fn home(&self) -> WINDOW {
    return button(self.start_y + 2, self.start_x + 18, "H");
  }

  fn ok(&self) -> WINDOW {
    return button(self.start_y + 8, self.start_x + 10, "0");
  }

  fn info(&self) -> WINDOW {
    return button(self.start_y + 15, self.start_x + 18, "*");
  }

  pub fn netflix(&self) -> WINDOW {
    return button(self.start_y + 18, self.start_x + 2, "N");
  }

  pub fn youtube(&self) -> WINDOW {
    return button(self.start_y + 18, self.start_x + 18, "Y");
  }
}
