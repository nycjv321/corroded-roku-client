extern crate reqwest;
extern crate ncurses;
extern crate log;
extern crate ssdp;

mod http;
mod roku;
mod cli;
mod config;
mod search;

use roku::remote::RokuRemote;
use http::SimpleHttpClient;
use cli::remote_control::{RemoteControlWindowBuilder, Directions};
use ncurses::*;


fn main() {
//    let client = SimpleHttpClient { url: config::url() };
    let client = SimpleHttpClient { url: search::roku_box() };
    let remote = RokuRemote::create(&client);
    let ui = RemoteControlWindowBuilder::initialize().build(&remote);
    ui.read_user_input();
    ui.exit();
}

