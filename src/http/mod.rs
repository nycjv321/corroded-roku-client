use super::reqwest;

pub struct SimpleHttpClient {
    pub url: String,
}

impl SimpleHttpClient {
    fn generate_url(&self, path: &str) -> String {
        let mut new_url = self.url.clone();
        new_url.push_str(path);
        return new_url;
    }
    pub fn get(&self, path: &str) -> String {
        return reqwest::get(self.generate_url(path).as_str()).expect("unable to retrieve uri")
            .text().expect("unable to parse body");
    }

    pub fn post(&self, path: &str) {
        let client = reqwest::Client::new();
        client.post(self.generate_url(path).as_str()).send().expect("post request failed");
    }
}


