use std::env;

fn host() -> String {
    return env::var("ROKU_BOX_HOSTNAME").unwrap_or(String::from("192.168.1.205"));
}

fn port() -> String {
    return env::var("ROKU_BOX_PORT").unwrap_or(String::from("8060"));
}

fn protocol() -> String {
    return env::var("ROKU_BOX_PROTOCOL").unwrap_or(String::from("http"));
}

pub fn url() -> String {
    return format!("{}://{}:{}", protocol(), host(), port());
}