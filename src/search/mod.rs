use super::ssdp::{listen, ResponseHeaders};
use super::log;

use log::{debug, warn};

use std::thread;
use std::sync::mpsc;



pub fn roku_box() -> String {
    let (tx, rx) = mpsc::channel();


    thread::spawn(move || {
        let determine_location = |response_headers: &ResponseHeaders| {
            if response_headers.server.to_lowercase().contains("roku") {
              match tx.send(response_headers.location.clone()) {
                Err(err) => {
                  log::error!("{}", err);
                }
                _ => {}
              }
            }
        };
        ssdp::listen(determine_location);
    });

    return String::from(rx.recv().unwrap().as_str());
}
