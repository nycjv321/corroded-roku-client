use http::SimpleHttpClient;
use roku::client::Client as RokuClient;

pub struct RokuRemote<'a> {
    pub client: &'a SimpleHttpClient,
    config: RokuRemoteConfig,
}

struct RokuRemoteConfig {
    youtube: String,
    netflix: String,
}

impl<'a> RokuRemote<'a> {
    pub fn create(client: &'a SimpleHttpClient) -> RokuRemote<'a> {
        let roku_client = RokuClient {client: &client };
        let netflix_id = roku_client.get_netflix_id();
        let youtube_id = roku_client.get_youtube_id();
        let config = RokuRemoteConfig { netflix: netflix_id, youtube: youtube_id };
        let remote = RokuRemote { client: &client, config };

        return remote;
    }

    pub fn youtube(&self) {
        let youtube = self.config.youtube.as_str();
        let launch_path = "/launch/";
        let path = launch_path.to_owned() + youtube;
        self.client.post(path.as_str());
    }

    pub fn netflix(&self) {
        let netflix = self.config.netflix.as_str();
        let launch_path = "/launch/";
        let path = launch_path.to_owned() + netflix;
        self.client.post(path.as_str());
    }


    pub fn play(&self) {
        self.client.post("/keypress/Play");
    }

    pub fn home(&self) {
        self.client.post("/keypress/Home");
    }

    pub fn back(&self) {
        self.client.post("/keypress/Back");
    }

    pub fn enter(&self) {
        self.client.post("/keypress/Select");
    }


    pub fn up(&self) {
        self.client.post("/keypress/Up");
    }

    pub fn down(&self) {
        self.client.post("/keypress/Down");
    }

    pub fn right(&self) {
        self.client.post("/keypress/Right");
    }

    pub fn left(&self) {
        self.client.post("/keypress/Left");
    }
    pub fn info(&self) {
        self.client.post("/keypress/Info");
    }

}