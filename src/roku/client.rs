extern crate minidom;

use self::minidom::Element;
use http::SimpleHttpClient;

pub struct Client<'a> {
    pub client: &'a SimpleHttpClient,
}

impl<'a> Client<'a> {
    pub fn get_id(&self, name: String) -> String {
        let text = self.client.get("/query/apps");
        let root: Element = text.parse().unwrap();
        for child in root.children() {
            if child.name() == "app" && child.text() == name {
                let child_id = child.attr("id").unwrap();
                let id: String = child_id.parse().unwrap();
                return id;
            }
        }
        return String::from("");

    }

    pub fn get_netflix_id(&self) -> String {
       return self.get_id( "Netflix".parse().unwrap());
    }

    pub fn get_youtube_id(&self) -> String {
        return self.get_id( "YouTube".parse().unwrap());
    }
}
