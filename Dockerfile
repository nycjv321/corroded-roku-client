#FROM rust:1.31.1-stretch
FROM rustlang/rust:nightly-slim

WORKDIR /usr/src/corroded-roku-client
COPY . .

RUN apt-get update -y && apt-get install libncurses5-dev libssl-dev pkg-config -y
RUN cargo install --path .

CMD ["corroded-roku-client"]
